package com.zuitt.capstone.services;


import com.zuitt.capstone.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);

    void createUser(User user);

}
