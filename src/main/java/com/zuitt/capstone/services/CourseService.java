package com.zuitt.capstone.services;


import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {

    void createCourse (String stringToken, Course course);

    //Get all courses in the database
    Iterable<Course> getAllCourses();

    //Get all courses of a specific user
    Iterable<Course> getMyCourses(String stringToken);

    ResponseEntity updateCourse (Long courseID, Course course, String stringToken);

    ResponseEntity deleteCourse (Long courseID, String stringToken);


    //Stretch Goal - Retrieve Single Course
    Course getCourse(Long courseID, String stringToken);



}
