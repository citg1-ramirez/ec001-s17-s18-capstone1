package com.zuitt.capstone.services;

import com.zuitt.capstone.config.JwtToken;
import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.repositories.CourseRepository;
import com.zuitt.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;


    @Override
    public void createCourse(String stringToken, Course course) {

        User enrollee = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Course newCourse = new Course();
        newCourse.setName(course.getName());
        newCourse.setDescription(course.getDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(enrollee);

        courseRepository.save(newCourse);
    }

    //Get all courses in the database
    @Override
    public Iterable<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    //Get all courses of a specific user
    public Iterable<Course> getMyCourses(String stringToken){
       User enrolledUser = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return enrolledUser.getCourses();
    }


    @Override
    public ResponseEntity updateCourse(Long courseID, Course course, String stringToken) {

        Course courseForUpdating = courseRepository.findById(courseID).get();
        String courseEnrolledName = courseForUpdating.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);


        if (authenticatedUsername.equals(courseEnrolledName)) {
            courseForUpdating.setName(course.getName());
            courseForUpdating.setDescription(course.getDescription());
            courseForUpdating.setPrice(course.getPrice());

            courseRepository.save(courseForUpdating);

            return new ResponseEntity("Course ID # " + courseID + " updated successfully.", HttpStatus.OK);
        }
        else{
            return  new ResponseEntity("You are not authorized to update Course ID # " + courseID +".", HttpStatus.UNAUTHORIZED);
        }
    }


    @Override
    public ResponseEntity deleteCourse(Long courseID, String stringToken) {
        Course courseForDeleting = courseRepository.findById(courseID).get();
        String courseEnrolledName = courseForDeleting.getUser().getUsername();
        String authenticatedUsername = jwtToken.getUsernameFromToken(stringToken);

        if (authenticatedUsername.equals(courseEnrolledName)) {
            courseRepository.deleteById(courseID);
            return new ResponseEntity("Course ID # " + courseID + " successfully deleted.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity("You are not authorized to delete Course ID # " + courseID +".", HttpStatus.UNAUTHORIZED);
        }
    }

    //Stretch Goal - Retrieve Single Course
    @Override
    public Course getCourse(Long courseID, String stringToken) {
        User enrolledUser = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Set<Course> retreivedCourses = enrolledUser.getCourses();

        Course foundCourse = new Course();

        for (Course course : retreivedCourses) {
            if (course.equals(courseRepository.findById(courseID).get())) {
                foundCourse = course;
                break;
            }
        }
        return foundCourse;
    }



}

