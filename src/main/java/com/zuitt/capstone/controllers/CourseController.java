package com.zuitt.capstone.controllers;

import com.zuitt.capstone.models.Course;
import com.zuitt.capstone.models.User;
import com.zuitt.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {

    @Autowired
    CourseService courseService;


    @RequestMapping(value = "/courses", method = RequestMethod.POST)
    public ResponseEntity<Object> createCourse(@RequestHeader(value = "authorization") String stringToken, @RequestBody Course course){

        courseService.createCourse(stringToken, course);

        return new ResponseEntity<>("Course added successfully.", HttpStatus.CREATED);
    }

    //Get all courses in the database
    @RequestMapping(value = "/courses", method = RequestMethod.GET)
    public ResponseEntity<Object> getAllCourses (){
        return new ResponseEntity<>(courseService.getAllCourses(), HttpStatus.OK);
    }

    //Get all courses of a specific user
    @RequestMapping(value = "/myCourses", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }

    @RequestMapping(value = "/courses/{courseID}", method = RequestMethod.PUT)
    public ResponseEntity updateCourse(@PathVariable Long courseID, @RequestBody Course course, @RequestHeader(value = "Authorization") String stringToken) {

        return courseService.updateCourse(courseID, course, stringToken);
    }


    @RequestMapping(value = "/courses/{courseID}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCourse(@PathVariable Long courseID,  @RequestHeader(value = "Authorization") String stringToken) {

        return courseService.deleteCourse(courseID, stringToken);
    }


    //Stretch Goal - Retrieve Single Course
    @RequestMapping(value = "/courses/{courseID}", method = RequestMethod.GET)
    public ResponseEntity<Object> getCourse(@PathVariable Long courseID, @RequestHeader(value = "Authorization") String stringToken) {
            return new ResponseEntity<>(courseService.getCourse(courseID, stringToken), HttpStatus.OK);
    }
}
