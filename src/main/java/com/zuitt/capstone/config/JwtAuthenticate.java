package com.zuitt.capstone.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

public class JwtAuthenticate implements AuthenticationEntryPoint, Serializable{
    private static final long serialVersionUID = 2113496612861483627L;

    @Override
    // This will handle the authentication failures.
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         // AuthenticationException is a class in Spring Security that contains all exceptions related to an Authentication object being invalid
                         AuthenticationException authException) throws IOException {
        // This will send a HTTP error response with a 401 status code with an "Unauthorized" message to the client.
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");

    }
}
